package preparation;


//This class keeps track of each neighbours index, and their next neighbour.
public class Neighbour {
    int neighbourId;
    Neighbour next;

    public Neighbour(int neighbourId, Neighbour neighbour) {
        this.neighbourId = neighbourId;
        next = neighbour;
    }
}
