package preparation;

class Edge {

    private int startVertex;
    private int endVertex;
    private int distance;

    int getStartVertex() {
        return startVertex;
    }

    void setStartVertex(int startVertex) {
        this.startVertex = startVertex;
    }

    int getEndVertex() {
        return endVertex;
    }

    void setEndVertex(int endVertex) {
        this.endVertex = endVertex;
    }

    int getDistance() {
        return distance;
    }

    void setDistance(int distance) {
        this.distance = distance;
    }
}
