package preparation;


import java.util.ArrayList;

//This class keeps track of each bus stop name and a list of their neighbours.
class Vertex {

    private String name;
    Neighbour listOfNeighbours;
    //private int vertexId;
    //private ArrayList<Integer> neighbours;

    public Vertex(String name, Neighbour neighbours) {
        this.name = name;
        this.listOfNeighbours = neighbours;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    /*int getVertexId() {
        return vertexId;
    }*/

    /*
    public void setVertexId(int vertexId) {
        this.vertexId = vertexId;
    }*/

   /* public ArrayList<Integer> getNeighbours() {
        return neighbours;
    }*/

   /* public void setNeighbours(ArrayList<Integer> neighbours) {
        this.neighbours = neighbours;
    }*/
}
