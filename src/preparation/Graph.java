package preparation;

import java.util.ArrayList;
import java.util.Random;


class Graph {
    //private ArrayList<Vertex> vertices = new ArrayList<>();
    //private ArrayList<Edge> edges = new ArrayList<>();
    //private Scanner sc = new Scanner(System.in);
    Vertex[] vertices;
    private String[] names = {"Bellevue", "SKF", "Gamlestaden", "Centralstationen", "Redbergsplatsen", "Brunnsparken", "Kungsportsplatsen", "Svingeln", "Olskroken", "Domkyrkan"};
    private Random random = new Random();


    Graph() {
        vertices = new Vertex[10];
        //enterGraphData();
        //printGraph(vertices, edges);

        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = new Vertex(names[i], null);
        }

        int i = 0;
        int j = 1;

        while (i < vertices.length) {

            int vertexOne = obtainNameIndex(names[i]);
            int vertexTwo = obtainNameIndex(names[j]);

            vertices[vertexOne].listOfNeighbours = new Neighbour(vertexTwo, vertices[vertexOne].listOfNeighbours);
            vertices[vertexTwo].listOfNeighbours = new Neighbour(vertexOne, vertices[vertexTwo].listOfNeighbours);

            i++;
            if (j < vertices.length-1) {
                j++;
            }
            else {
                j = 0;
            }
        }
    }

    private int obtainNameIndex(String name) {
        for (int i = 0; i < vertices.length; i++) {
            if (vertices[i].getName().equals(name)){
                return i;
            }
        }
        return -1;
    }

    public void print() {
        System.out.println();
        for (int i = 0; i < vertices.length; i++) {
            System.out.println(vertices[i].getName());
            for (Neighbour nbr = vertices[i].listOfNeighbours; nbr != null; nbr = nbr.next){
                System.out.println("--> " + vertices[nbr.neighbourId].getName());
            }
            System.out.println("\n");
        }
    }

    //TODO: there should be 10 nodes.
   /* private void enterGraphData() {
        int numberOfVertices = 10;
        for (int i = 0; i < numberOfVertices; i++) {
            createVertices(i, vertices);
        }
        //TODO: loop through all vertices
        determineConnections(vertices.get(0));
        determineConnections(vertices.get(1));
        enterEdgesData();
    }

    //Create a list of vertices
    private void createVertices(int currentIndex, ArrayList<Vertex> vertices) {
        vertices.add(new Vertex());
        vertices.get(currentIndex).setVertexId(currentIndex);
        vertices.get(currentIndex).setName(names[currentIndex]);
    }

    //Determine what other vertices each vertex is connected to TODO: should there always be 2? Or random 2 or 3?
    private void determineConnections(Vertex currentVertex) {
        System.out.println("Determining neighbouring vertices for vertex " + currentVertex.getName());

        //Determining neighbour one, making sure it's not the current vertex and //TODO should also not already have 3 neighbours
        int firstNeighbour = (int)(Math.random() * 10);
        System.out.println("First neighbour: " + firstNeighbour);
        while (currentVertex.getVertexId() == firstNeighbour){
            System.out.println("Current vertex and potential neighbour are same, pick a new number");
            firstNeighbour = (int)(Math.random() * 10);
            System.out.println("New neighbour " + firstNeighbour);
        }
        System.out.println("Neighbour to be set: " + vertices.get(firstNeighbour).getName());



        currentVertex.setNeighbours(new ArrayList<>());
        currentVertex.getNeighbours().add(firstNeighbour);

        System.out.println("Neighbours: " + currentVertex.getNeighbours());

        //Determine who is neighbour nr 2. Making sure it's not same as neighbour 1 or starting point.
        int secondNeighbour = (int)(Math.random() * 10);
        System.out.println("Second neighbour: " + secondNeighbour);

        while (secondNeighbour == firstNeighbour || secondNeighbour == currentVertex.getVertexId()) {
            System.out.println("Same as neighbour 1 or starting point, picking a new vertex.");
            secondNeighbour = (int)(Math.random() * 10);
            System.out.println("New neighbour: " + secondNeighbour);
        }

        System.out.println("Neighbour to be set: " + secondNeighbour);

        //Adding neighbour to list of neighbours
        currentVertex.getNeighbours().add(secondNeighbour);
        System.out.println("Neighbours: " + currentVertex.getNeighbours());
        System.out.println("Current: ");
        printGraph(vertices, edges);
    }

    //Entering random edge data and making sure start and end vertex are not same.
    private void enterEdgesData() {
        int numberOfEdges = 3;
        for (int i = 0; i < numberOfEdges; i++){

            System.out.println("\nCreating edge " + i + "\n");
            edges.add(new Edge());
            edges.get(i).setStartVertex((int)(Math.random()*10));
            System.out.println("Start vertex: " + edges.get(i).getStartVertex());
            edges.get(i).setEndVertex((int)(Math.random()*10));
            System.out.println("End vertex: " + edges.get(i).getEndVertex());

            while (edges.get(i).getStartVertex() == edges.get(i).getEndVertex()){
                System.out.println("Same starting and ending point, picking new random vertex");
                edges.get(i).setEndVertex((int)(Math.random()*10));
                System.out.println("New end vertex: " + edges.get(i).getEndVertex());
            }

            edges.get(i).setDistance((int)(Math.random()*10));
            System.out.println("Distance: " + edges.get(i).getDistance());
        }
    }

    private void printGraph(ArrayList<Vertex> vertices, ArrayList<Edge> edges) {
        System.out.println("\nVertices\n");
        for (Vertex v : vertices) {
            System.out.println("Vertex index: " + v.getVertexId() + "   Name: " + v.getName());// + "   Neighbours: " + vertices.get(v.getNeighbours().get(0)).getName());
            if(vertices.get(v.getNeighbours().get(0)).getName() != null && vertices.get(v.getNeighbours().get(1)).getName() != null){
                System.out.println("   Neighbours: " + vertices.get(v.getNeighbours().get(0)).getName());
            }
        }

        System.out.println("\nEdges\n");
        for (int i = 0; i < edges.size(); i++) {
            System.out.println("Edge nr " + i);
            System.out.println("Starting point: " + edges.get(i).getStartVertex());
            System.out.println("Ending point: " + edges.get(i).getEndVertex());
            System.out.println("Distance: " + edges.get(i).getDistance() + "\n");
        }
    }*/

}
