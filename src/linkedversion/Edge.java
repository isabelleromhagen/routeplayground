package linkedversion;

public class Edge {
    private int startVertex;
    private int endVertex;
    private int distance;



    //Getters and setters

    public int getStartVertex() {
        return startVertex;
    }

    void setStartVertex(int startVertex) {
        this.startVertex = startVertex;
    }

    public int getEndVertex() {
        return endVertex;
    }

    void setEndVertex(int endVertex) {
        this.endVertex = endVertex;
    }

    public int getDistance() {
        return distance;
    }

    void setDistance(int distance) {
        this.distance = distance;
    }
}
