package linkedversion;

public class Vertex {
    private int vertex;
    private String name;



    //Getters and setters

    public int getVertex() {
        return vertex;
    }

    public void setVertex(int vertex) {
        this.vertex = vertex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
