package linkedversion;


import java.util.LinkedList;
import java.util.Scanner;

class Graph {
    private LinkedList<Vertex> vertices = new LinkedList<>();
    private LinkedList<Edge> edges = new LinkedList<>();
    private Scanner sc = new Scanner(System.in);
    private String[] names = {"Bellevue", "SKF", "Gamlestaden", "Centralstationen", "Redbergsplatsen"};

    Graph() {
        enterGraphData();
        printGraph(vertices, edges);
    }

    private void enterGraphData() {
        int numberOfVertices = 3;
        for (int i = 0; i < numberOfVertices; i++) {
            enterVerticesData(i);
        }
        enterEdgesData();
    }

    private void enterVerticesData(int currentIndex) {
        vertices.add(new Vertex());
        vertices.get(currentIndex).setVertex(currentIndex);
        vertices.get(currentIndex).setName(names[currentIndex]);

    }


    private void enterEdgesData() {
        int numberOfEdges = 3;
        for (int i = 0; i < numberOfEdges; i++){

            System.out.println("\nCreating edge " + i + "\n");
            edges.add(new Edge());
            System.out.println("Enter start vertex: ");
            int startInput = sc.nextInt();
            edges.get(i).setStartVertex(startInput);
            System.out.println("Enter end vertex: ");
            int endInput = sc.nextInt();
            edges.get(i).setEndVertex(endInput);
            System.out.println("Enter distance: ");
            int distanceInput = sc.nextInt();
            edges.get(i).setDistance(distanceInput);

        }
    }

    private void printGraph(LinkedList<Vertex> vertices, LinkedList<Edge> edges) {
        System.out.println("\nVertices\n");
        for (Vertex v : vertices) {
            System.out.println("Vertex index: " + v.getVertex() + "   Name: " + v.getName());
        }

        System.out.println("\nEdges\n");
        for (int i = 0; i < edges.size(); i++) {
            System.out.println("Edge nr " + i);
            System.out.println("Starting point: " + edges.get(i).getStartVertex());
            System.out.println("Ending point: " + edges.get(i).getEndVertex());
            System.out.println("Distance: " + edges.get(i).getDistance() + "\n");
        }
    }

   //Getters and setters

    public LinkedList<Vertex> getVertices() {
        return vertices;
    }

    public void setVertices(LinkedList<Vertex> vertices) {
        this.vertices = vertices;
    }

    public LinkedList<Edge> getEdges() {
        return edges;
    }

    public void setEdges(LinkedList<Edge> edges) {
        this.edges = edges;
    }
}

