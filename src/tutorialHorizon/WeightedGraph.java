package tutorialHorizon;

import java.util.LinkedList;

public class WeightedGraph {
    static class Edge {
        int source;
        int destination;
        int weight;

        public Edge(int source, int destination, int weight) {
            this.source = source;
            this.destination = destination;
            this.weight = weight;
        }
    }

    static class Graph {
        int numberOfVertices;
        LinkedList<Edge> [] neighbours;

        Graph(int numberOfVertices) {
            this.numberOfVertices = numberOfVertices;
            neighbours = new LinkedList[numberOfVertices];
            //initialize adjacency lists for all the vertices
            for (int i = 0; i <numberOfVertices ; i++) {
                neighbours[i] = new LinkedList<>();
            }
        }

        public void addEdge(int source, int destination, int weight) {
            Edge edge = new Edge(source, destination, weight);
            neighbours[source].addFirst(edge);
            neighbours[destination].addFirst(edge);
        }

        public void printGraph(){
            for (int i = 0; i < numberOfVertices; i++) {
                LinkedList<Edge> list = neighbours[i];
                for (int j = 0; j <list.size() ; j++) {
                    System.out.println("vertex " + i + " is connected to " +
                            list.get(j).destination + " with weight " +  list.get(j).weight);
                }
            }
        }
    }
    public static void main(String[] args) {
        int vertices = 10;
        Graph graph = new Graph(vertices);
        graph.addEdge(0, 1, 10);
        graph.addEdge(0, 3, 20);
        graph.addEdge(1, 2, 5);
        graph.addEdge(1, 7, 30);
        graph.addEdge(2, 3, 15);
        graph.addEdge(4, 5, 50);
        graph.addEdge(4, 6, 40);
        graph.addEdge(5, 8, 25);
        graph.addEdge(6, 7, 35);
        graph.addEdge(7, 9, 45);
        graph.addEdge(8, 9, 60);
        graph.printGraph();
    }
}