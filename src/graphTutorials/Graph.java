package graphTutorials;

class Neighbour {
    public int vertexNum;
    public Neighbour next;
    public Neighbour(int vnum, Neighbour nbr) {
        this.vertexNum = vnum;
        next = nbr;
    }
}

//This class keeps track of each bus stop name and a list of their neighbours.
class Vertex {
    String name;
    Neighbour listOfNeighbours;
    Vertex(String name, Neighbour neighbors) {
        this.name = name;
        this.listOfNeighbours = neighbors;
    }
}

/**
 * @author Sesh Venugopal. May 31, 2013.
 */
public class Graph {

    Vertex[] vertices;
    String[] names = {"Bellevue", "SKF", "Gamlestaden", "Centralstationen", "Redbergsplatsen", "Brunnsparken", "Kungsportsplatsen", "Svingeln", "Olskroken", "Domkyrkan"};

   // public Graph(String file) throws FileNotFoundException {
    public Graph() {

       // Scanner sc = new Scanner(new File(file));
        //vertices = new Vertex[sc.nextInt()];
        vertices = new Vertex[10];

        // read vertices
        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = new Vertex(names[i], null);
        }

        int i = 0;
        int j = 1;
        // read edges
       // while (sc.hasNext()) {
        while (i < vertices.length) {

            // read vertex names and translate to vertex numbers
            //int v1 = indexForName(sc.next());
            //int v2 = indexForName(sc.next());
            int v1 = indexForName(names[i]);
            int v2 = indexForName(names[j]);

            // add v2 to front of v1's adjacency list and
            // add v1 to front of v2's adjacency list
            vertices[v1].listOfNeighbours = new Neighbour(v2, vertices[v1].listOfNeighbours);
            vertices[v2].listOfNeighbours = new Neighbour(v1, vertices[v2].listOfNeighbours);
            i++;
            if (j < vertices.length-1){
                j++;
            }
            else {
                j = 0;
            }
        }
    }

    int indexForName(String name) {
        for (int v = 0; v < vertices.length; v++) {
            if (vertices[v].name.equals(name)) {
                return v;
            }
        }
        return -1;
    }

    public void print() {
        System.out.println();
        for (int v = 0; v < vertices.length; v++) {
            System.out.print(vertices[v].name);
            for (Neighbour nbr = vertices[v].listOfNeighbours; nbr != null; nbr=nbr.next) {
                System.out.print(" --> " + vertices[nbr.vertexNum].name);
            }
            System.out.println("\n");
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        //Scanner sc = new Scanner(System.in);
        //System.out.print("Enter graph input file name: ");
        //String file = sc.nextLine();
        //Graph graph = new Graph(file);
        Graph graph = new Graph();
        graph.print();

    }

}
